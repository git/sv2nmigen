import sys

import lexor
import parse_sv
import absyn
import pypreproc

from ply import *
import os

if __name__ == '__main__':
    fname = sys.argv[1]
    outputfn = os.path.splitext(fname)[0]+'.py'
    print(outputfn)
    with open(fname) as f:
        data = f.read()
        preproc = pypreproc.Preprocessor()
        data = preproc.removeComments(data)
        parse_sv.absyn = absyn.Absyn(outputfn)
        yacc.parse(data, debug=parse_sv.yacc2_debug)
        print("No Error")
        parse_sv.absyn.appendComments(preproc.insertDocstrings(data))
